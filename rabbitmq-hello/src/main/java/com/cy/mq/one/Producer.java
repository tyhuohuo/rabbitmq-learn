package com.cy.mq.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * 生产者
 * @author huohuo
 * @create: 2021/7/1 14:20
 */
public class Producer {
    // 队列名称
    private static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建连接
        ConnectionFactory factory = new ConnectionFactory();
        //ip 连接rabbitmq的队列
        factory.setHost("192.168.137.10");
        //用户名密码
        factory.setUsername("huohuo");
        factory.setPassword("123");
        //连接
        Connection connection = factory.newConnection();
        //获取信道
        Channel channel = connection.createChannel();
        /**
         *	生成一个队列
         *	1.队列名称
         *	2.队列里面的消息是否持久化 默认消息存储在内存中
         *	3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
         *	4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
         *	5.其他参数
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        String msg = "hello world!";
        /**
         *	发送一个消息
         *	1.发送到那个交换机
         *	2.路由的 key 是哪个
         *	3.其他的参数信息
         *	4.发送消息的消息体
         */
        channel.basicPublish("",QUEUE_NAME,null,msg.getBytes(StandardCharsets.UTF_8));
        System.out.println("消息发送成功");

    }
}
