package com.cy.mq.utils;

/**
 * @author huohuo
 * @create: 2021/7/1 15:50
 */
public class SleepUtils {
    public static void sleep(int second) {
        try {
            Thread.sleep(1000 * second);
        } catch (InterruptedException _ignored) {
            Thread.currentThread().interrupt();
        }

    }
}
