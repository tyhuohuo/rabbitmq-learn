package com.cy.mq.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author huohuo
 * @create: 2021/7/1 14:57
 */
public class RabbitMqUtils {
    //得到一个连接的 channel
    public static Channel getChannel() throws Exception {
        //创建一个连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.137.10");
        factory.setUsername("huohuo");
        factory.setPassword("123");
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

}
