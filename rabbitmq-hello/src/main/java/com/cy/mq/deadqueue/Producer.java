package com.cy.mq.deadqueue;

import com.cy.mq.utils.RabbitMqUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

/**
 * @author huohuo
 * @create: 2021/7/6 9:12
 */
public class Producer {
    private static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] argv) throws Exception {
        try (Channel channel = RabbitMqUtils.getChannel()) {
            channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
            // 设置消息的 TTL 时间,10s 未消费，进入死信队列
            //AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();
            // 该信息是用作演示队列个数限制
            for (int i = 1; i < 11; i++) {
                String message = "info" + i;
                // ttl属性
                //channel.basicPublish(NORMAL_EXCHANGE, "zhangsan", properties, message.getBytes());
                // 模拟队列最大长度
                channel.basicPublish(NORMAL_EXCHANGE, "zhangsan", null, message.getBytes());
                System.out.println(" 生产者发送消息:" + message);
            }
        }
    }
}
